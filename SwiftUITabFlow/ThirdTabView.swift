//
//  ThirdTabView.swift
//  SwiftUITabFlow
//
//  Created by N Sh on 06.03.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct ModalView: View{
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var body: some View {
        Group{
            Text("Modal View")
            Button(action: {self.presentationMode.wrappedValue.dismiss()}){
                Text("Dismiss")
            }
        }
    }
}
struct ThirdTabView: View {
    
    @State var ShowModul:Bool = false
    
    var body: some View {
        Button(action: {
            self.ShowModul = true
        }){
            Text("Show Modal")
        }.sheet(isPresented: self.$ShowModul){
            ModalView()
        }
    }
}

struct ThirdTabView_Previews: PreviewProvider {
    static var previews: some View {
        ThirdTabView()
    }
}
