//
//  ContentView.swift
//  SwiftUITabFlow
//
//  Created by N Sh on 29.02.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI


final class SecondTabViewModel: ObservableObject{
    @Published private(set) var dataItems = ["First",
                                             "Second",
                                             "Third",
                                             "Fourth"
    ]
}

struct SimpleView: View{
    var name: String
    
    var body:some View{
        Text(name + " View")
    }
}


struct SecondTabView: View {
    @EnvironmentObject var secondViewModel: SecondTabViewModel
    @Binding var tabItem: Int
    @Binding var selectedItem: Int?
    
    var body: some View {
        NavigationView{
                List{
                    ForEach(0..<self.secondViewModel.dataItems.count){i in
                        NavigationLink(destination: SimpleView(name: self.secondViewModel.dataItems[i]), tag: self.tabItem, selection: self.$selectedItem){
                            Text(self.secondViewModel.dataItems[i])
                        }
                }
            }.navigationBarTitle("List")
        }
    }
}

struct FirstTabView: View {
    
    @Binding var tabItem:Int
    @Binding var selectedItem:Int?
    
    var body: some View {
            Button(action: {
                self.tabItem = 1
                self.selectedItem = 1
            }, label: {Text("Click me")})
        }
    }

struct ContentView: View {
    
    @State var tabItem:Int = 0
    @State var selectedItem:Int?
    
    var body: some View {
        TabView(selection: $tabItem){
            FirstTabView(tabItem: $tabItem, selectedItem: $selectedItem)
                .tabItem({Text("First")})
                .tag(0)
            SecondTabView(tabItem: $tabItem, selectedItem: $selectedItem)
                .environmentObject(SecondTabViewModel())
                .tabItem({Text("Second")})
                .tag(1)
            ThirdTabView()
                .tabItem({Text("Third")})
                .tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

